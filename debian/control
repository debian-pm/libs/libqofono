Source: libqofono
Maintainer: Ken VanDine <ken.vandine@ubuntu.com>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libqt5xmlpatterns5-dev,
               pkg-config,
               qml-module-qtquick2,
               qtbase5-dev,
               qtchooser,
               qtdeclarative5-dev
Standards-Version: 4.5.0
Vcs-Browser: https://gitlab.com/debian-pm/libs/libqofono
Vcs-Git: https://gitlab.com/debian-pm/libs/libqofono.git
Homepage: https://git.merproject.org/mer-core/libqofono

Package: libqofono-qt5-0
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: ofono (>= 1.16)
Pre-Depends: ${misc:Pre-Depends}
Description: Qt library for Ofono
 A library for accessing the ofono daemon, and a declarative plugin for
 it. This allows accessing ofono in qtquick and friends.

Package: libqofono-dev
Architecture: any
Section: libdevel
Depends: libqofono-qt5-0 (= ${binary:Version}), ${misc:Depends}
Description: Qt library for Ofono
 A library for accessing the ofono daemon, and a declarative plugin for
 it. This allows accessing ofono in qtquick and friends.

Package: qml-module-ofono
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Breaks: qtdeclarative5-ofono0.2, ubuntu-system-settings (<< 0.3+15.04.20150114)
Replaces: qtdeclarative5-ofono0.2
Description: QML bindings for libqofono
 A library for accessing the ofono daemon, and a declarative plugin for
 it. This allows accessing ofono in qtquick and friends.

Package: libqofono-tests
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: QML bindings for libqofono
 A library for accessing the ofono daemon, and a declarative plugin for
 it. This allows accessing ofono in qtquick and friends.
 This package contains tests.
